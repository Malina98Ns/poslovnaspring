package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.KursnaLista;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KursnaListaDTO {

    private long id;
    private Date datum;
    private String broj;
    private Date datumPrimene;

    private BankaDTO banka;

    private List<KursUValutiDTO> kurseviUValutama = new ArrayList<>();

    public KursnaListaDTO(KursnaLista obj) {
        id = obj.getId();
        datum = obj.getDatum();
        broj = obj.getBroj();
        datumPrimene = obj.getDatumPrimene();
        if(obj.getBanka() != null)
            banka = new BankaDTO(obj.getBanka());
    }
}
