set foreign_key_checks = 0;

truncate table korisnik;
truncate table klijent;
truncate table delatnost;
truncate table drzava;
truncate table valuta;
truncate table banka;
truncate table racun;
truncate table dnevno_stanje;
truncate table kursuvaluti;
truncate table medjubankarski_prenos;
truncate table nalog;
truncate table poruka;
truncate table ukidanje_racuna;


INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Privatan biznis', 'PBB');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Fabrika', 'FAB');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Softver developer', 'SFD');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Prehrambena Industrija', 'PHI');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Apatinska Pivara', 'APP');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Porno Mafija', 'PRM');

INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Srbija', 'SRB');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Hrvatska', 'HRT');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Crna Gora', 'CRG');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Bosna i Hercegovina', 'BIH');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Svajcarska', 'SWI');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Rumunija', 'ROU');

INSERT INTO `ebank`.`valuta` (`naziv`, `sifra`, `drzava_id`) VALUES ('Dinar', 'RSD', 1);
INSERT INTO `ebank`.`valuta` (`naziv`, `sifra`, `drzava_id`) VALUES ('Hrvatska kuna', 'HRK', 2);
INSERT INTO `ebank`.`valuta` (`naziv`, `sifra`, `drzava_id`) VALUES ('Evro', 'EUR', 3);
INSERT INTO `ebank`.`valuta` (`naziv`, `sifra`, `drzava_id`) VALUES ('BiH konvertibilna marka', 'BAM', 4);
INSERT INTO `ebank`.`valuta` (`naziv`, `sifra`, `drzava_id`) VALUES ('Svajcarski franak', 'CHF', 5);
INSERT INTO `ebank`.`valuta` (`naziv`, `sifra`, `drzava_id`) VALUES ('Rumunski lej', 'RON', 6);

INSERT INTO `ebank`.`banka` (`adresa`, `email`, `fax`, `naziv`, `obracunski_racun`, `sifra`, `swift`, `telefon`, `web`) VALUES ('Balzakova 45', 'tomidog@gmail.com', '9-888-777-6666', 'Tomijeva banka', '115-43209325794-78', 'TMB', 'TOMISRB', '069-123-321', 'bobcatcode.rs');
INSERT INTO `ebank`.`banka` (`adresa`, `email`, `fax`, `naziv`, `obracunski_racun`, `sifra`, `swift`, `telefon`, `web`) VALUES ('Stevana Decanskog 3', 'arsenal_car@mail.com', '1-222-333-4444', 'Ralici banka', '115-35435643642-78', 'RLB', 'RALKESRB', '069-456-321', 'bobcatcode.rs');
INSERT INTO `ebank`.`banka` (`adresa`, `email`, `fax`, `naziv`, `obracunski_racun`, `sifra`, `swift`, `telefon`, `web`) VALUES ('1 Novembra 427', 'zeljkola@gmail.com', '5-444-333-2222', 'Zeksova banka', '115-5435345435-78', 'ZKB', 'ZEKSSRB', '065-243-109', 'bobcatcode.rs');
INSERT INTO `ebank`.`banka` (`adresa`, `email`, `fax`, `naziv`, `obracunski_racun`, `sifra`, `swift`, `telefon`, `web`) VALUES ('Ljube Stankovica 91F', 'melisaaa@gmail.com', '1-555-234-5678', 'Malina banka', '115-42444223555-78', 'MLB', 'MLNSRB', '069-555-333', 'bobcatcode.rs');

INSERT INTO `ebank`.`klijent` (`adresa`, `ime`, `jmbg`, `odobren`, `prezime`, `telefon`, `tip_klijenta`, `delatnost_id`) VALUES ('Balzakova 45 Novi Sad', 'Dusan', '1986437888123',true, 'Zurkovic', '0606700791', '1', '1');
INSERT INTO `ebank`.`klijent` (`adresa`, `ime`, `jmbg`, `odobren`, `prezime`, `telefon`, `tip_klijenta`, `delatnost_id`) VALUES ('Indjija Decanskog', 'Rade', '1986437888123',true, 'Obradovic', '0631840561', '0', '2');
INSERT INTO `ebank`.`klijent` (`adresa`, `ime`, `jmbg`, `odobren`, `prezime`, `telefon`, `tip_klijenta`, `delatnost_id`) VALUES ('Boracko naselje', 'Kraljevacki', '1986437888123',true, 'Kosovljanin', '0653075829', '1', '3');

INSERT INTO `ebank`.`korisnik` (`korisnicko_ime`, `lozinka`, `uloga`,`klijent_id`) VALUES ('korisnik', '$2y$10$ec1b5eKcAK1VgnZhJOH0m.wAoH9bjrX3Xw6gHSve3PLQhcPKP59Dy', '1', '3');
INSERT INTO `ebank`.`korisnik` (`korisnicko_ime`, `lozinka`, `uloga`,`klijent_id`) VALUES ('zure', '$2y$10$t4535XQ.qlDEThch5SsGiulO6vMimbKSzAQnTEWEs.r8DQOIN5bwy', '1', '1');
INSERT INTO `ebank`.`korisnik` (`korisnicko_ime`, `lozinka`, `uloga`,`klijent_id`) VALUES ('rade', '$2y$10$EUpbSasyi3WEcsMuw0BDAu581LyD8Q.caCHJkbi9GPNoaRJtQzKW6', '1', '2');
INSERT INTO `ebank`.`korisnik` (`korisnicko_ime`, `lozinka`, `uloga`, `banka_id`) VALUES ('izvrsilac', '$2y$10$clEFFymzkRWSm7bGA2vLOOVzPwvyUAElWJ235fbGHcGPewAleEB5e', '2', '1');
INSERT INTO `ebank`.`korisnik` (`korisnicko_ime`, `lozinka`, `uloga`, `banka_id`) VALUES ('izvrsilacdva', '$2y$10$JLeSGTanD8U2vnV6/cnhQ.JkKZTHN05DyEN5SEVyR3oq.OBYpGFM2', '2', '2');
INSERT INTO `ebank`.`korisnik` (`korisnicko_ime`, `lozinka`, `uloga`) VALUES ('admin', '$2y$10$482BcEZ2LMqN1KlkfnsMhuOtVVyZk.egRqcq5kBVmsKI2UwfVivq2', '0');


UPDATE `ebank`.`klijent` SET `korisnik_id` = '2' WHERE (`id` = '1');
UPDATE `ebank`.`klijent` SET `korisnik_id` = '3' WHERE (`id` = '2');
UPDATE `ebank`.`klijent` SET `korisnik_id` = '1' WHERE (`id` = '3');

INSERT INTO `ebank`.`racun`
(`broj_racuna`, `datum_kreiranja`, `izbrisan`, `odobren`, `rezervisan_iznos`, `stanje`, `banka_id`, `klijent_id`, `valuta_id`)
VALUES ('1112223334445', '2021-01-01 15:13', false, true, 300, 9000, 1, 1, 1);
INSERT INTO `ebank`.`racun`
(`broj_racuna`, `datum_kreiranja`, `izbrisan`, `odobren`, `rezervisan_iznos`, `stanje`, `banka_id`, `klijent_id`, `valuta_id`)
VALUES ('5554443332', '2021-01-01 15:13', false, true, 300, 1000, 1, 2, 1);
INSERT INTO `ebank`.`racun`
(`broj_racuna`, `datum_kreiranja`, `izbrisan`, `odobren`, `rezervisan_iznos`, `stanje`, `banka_id`, `klijent_id`, `valuta_id`)
VALUES ('4443335551', '2021-01-01 15:13', false, true, 300, 8000, 2, 3, 1);
INSERT INTO `ebank`.`racun`
(`broj_racuna`, `datum_kreiranja`, `izbrisan`, `odobren`, `rezervisan_iznos`, `stanje`, `banka_id`, `klijent_id`, `valuta_id`)
VALUES ('111111111111111', '2021-04-07 20:13', false, true, 100, 300000, 1, 2, 1);
INSERT INTO `ebank`.`racun`
(`broj_racuna`, `datum_kreiranja`, `izbrisan`, `odobren`, `rezervisan_iznos`, `stanje`, `banka_id`, `klijent_id`, `valuta_id`)
VALUES ('222222222222222', '2021-04-07 21:13', false, true, 100, 5000, 1, 2, 1);

INSERT INTO `ebank`.`dnevno_stanje` (`broj_izvoda`, `datum_prometa`, `novo_stanje`, `prethodno_stanje`, `promet_na_teret`, `prometukorist`, `racun_id`) VALUES ('1', '2020-11-11 14:13', '8500', '9000', '700', '200', '1');
INSERT INTO `ebank`.`dnevno_stanje` (`broj_izvoda`, `datum_prometa`, `novo_stanje`, `prethodno_stanje`, `promet_na_teret`, `prometukorist`, `racun_id`) VALUES ('2', '2020-11-11 14:13', '200', '1000', '900', '100', '2');
INSERT INTO `ebank`.`dnevno_stanje` (`broj_izvoda`, `datum_prometa`, `novo_stanje`, `prethodno_stanje`, `promet_na_teret`, `prometukorist`, `racun_id`) VALUES ('3', '2020-11-11 14:13', '7000', '8000', '1200', '200', '3');

INSERT INTO `ebank`.`kursna_lista` (`broj`, `datum`, `datum_primene`, `banka_id`) VALUES ('1', '2020-11-11 15:30', '2020-11-20 15:30', '1');
INSERT INTO `ebank`.`kursna_lista` (`broj`, `datum`, `datum_primene`, `banka_id`) VALUES ('2', '2020-11-11 15:30', '2020-11-20 15:30', '2');

INSERT INTO `ebank`.`kursuvaluti` (`kupovni`, `prodajni`, `redni_broj`, `srednji`, `kursna_lista_id`, `osnovna_valuta_id`, `sporedna_valuta_id`) VALUES ('117.22', '117.92', '1', '117.57', '1', '1', '3');
INSERT INTO `ebank`.`kursuvaluti` (`kupovni`, `prodajni`, `redni_broj`, `srednji`, `kursna_lista_id`, `osnovna_valuta_id`, `sporedna_valuta_id`) VALUES ('15.43', '15.48', '2', '15.48', '2', '1', '2');

INSERT INTO `ebank`.`medjubankarski_prenos`
(`datum_prenosa`, `iznos`, `tip_transfera`, `valuta`, `banka_duznika_id`, `banka_primaoca_id`, `racun_duznika_id`, `racun_primaoca_id`) VALUES
 ('2021-11-03 15:30', '300', 1, 1, 1, 2, 1, 3);

 INSERT INTO `ebank`.`ukidanje_racuna` (`datum_kreiranja`, `obrazlozenje`, `zavrseno`, `racun_za_prenos_novca_id`, `racun_za_ukidanje_id`) VALUES ('2021-03-11', 'Sta da kazem', b'0', '5', '2');

