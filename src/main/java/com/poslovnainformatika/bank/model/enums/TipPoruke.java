package com.poslovnainformatika.bank.model.enums;

public enum TipPoruke {

	MT102,
	MT103,
	MT900,
	MT910
}