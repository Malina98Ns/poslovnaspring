package com.poslovnainformatika.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovnainformatika.bank.model.Valuta;

public interface ValutaRepository extends JpaRepository<Valuta, Long> {

}
