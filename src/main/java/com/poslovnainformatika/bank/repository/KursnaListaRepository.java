package com.poslovnainformatika.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovnainformatika.bank.model.KursnaLista;

public interface KursnaListaRepository extends JpaRepository<KursnaLista, Long> {

}
