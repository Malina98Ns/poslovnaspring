package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.MedjubankarskiPrenos;
import com.poslovnainformatika.bank.model.enums.TipTransfera;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MedjuBankarskiPrenosDTO {

    private long id;
    private double iznos;
    private String valuta;
    private TipTransfera tipTransfera;
    private Date datumPrenosa;

    private BankaDTO bankaDuznika;
    private BankaDTO bankaPrimaoca;
    private RacunDTO racunDuznika;
    private RacunDTO racunPrimaoca;

    public void MedjubankarskiPrenosDTO(MedjubankarskiPrenos m) {
        this.id = m.getId();
        this.iznos = m.getIznos();
        this.valuta = m.getValuta();
        this.tipTransfera = m.getTipTransfera();
        this.datumPrenosa = m.getDatumPrenosa();

        if(m.getBankaDuznika() != null) {
            this.bankaDuznika = new BankaDTO(m.getBankaDuznika());
        }
        if(m.getBankaPrimaoca() != null) {
            this.bankaPrimaoca = new BankaDTO(m.getBankaPrimaoca());
        }
        if(m.getRacunDuznika() != null) {
            this.racunDuznika = new RacunDTO(m.getRacunDuznika());
        }
        if(m.getRacunPrimaoca() != null) {
            this.racunPrimaoca = new RacunDTO(m.getRacunPrimaoca());
        }
    }
}
