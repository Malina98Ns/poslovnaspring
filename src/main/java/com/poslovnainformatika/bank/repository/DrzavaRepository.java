package com.poslovnainformatika.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovnainformatika.bank.model.Drzava;

public interface DrzavaRepository extends JpaRepository<Drzava, Long>{

}


