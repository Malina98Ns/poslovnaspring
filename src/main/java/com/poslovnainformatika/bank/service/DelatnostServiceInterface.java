package com.poslovnainformatika.bank.service;

import com.poslovnainformatika.bank.model.Delatnost;

import java.util.List;

public interface DelatnostServiceInterface {

    List<Delatnost> findAll();

    Delatnost findOne(long id);

    Delatnost save(Delatnost del);

    void remove(long id);

}
