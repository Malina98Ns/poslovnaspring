package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.model.Valuta;

public interface ValutaServiceInterface {

	List<Valuta> findAll();
	
	Valuta findOne(long id);
	
	Valuta save(Valuta valuta);
	
	void remove(long id);
	
	

	
}
