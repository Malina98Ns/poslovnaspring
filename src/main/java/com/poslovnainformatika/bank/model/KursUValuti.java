package com.poslovnainformatika.bank.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class KursUValuti {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = false)
    private long redniBroj;

    @Column(unique = false, nullable = false)
    private double kupovni;

    @Column(unique = false, nullable = false)
    private double srednji;

    @Column(unique = false, nullable = false)
    private double prodajni;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Valuta osnovnaValuta;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Valuta sporednaValuta;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private KursnaLista kursnaLista;
}