package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.Klijent;
import com.poslovnainformatika.bank.model.Racun;
import com.poslovnainformatika.bank.model.enums.TipKlijenta;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KlijentDTO {

    private long id;
    private String ime;
    private String prezime;
    private String jmbg;
    private String telefon;
    private String adresa;
    private TipKlijenta tipKlijenta;
    private boolean odobren;
    private KorisnikDTO korisnik;
    private DelatnostDTO delatnost;
    private List<RacunDTO> racuni = new ArrayList<>();

    public KlijentDTO(Klijent obj) {
        id = obj.getId();
        ime = obj.getIme();
        prezime = obj.getPrezime();
        jmbg = obj.getJmbg();
        telefon = obj.getTelefon();
        adresa = obj.getAdresa();
        odobren = obj.isOdobren();
        tipKlijenta = obj.getTipKlijenta();
        if(obj.getKorisnik() != null)
            korisnik = new KorisnikDTO(obj.getKorisnik());
        if(obj.getDelatnost() != null)
            delatnost = new DelatnostDTO(obj.getDelatnost());
    }
    
    public void setRacuniListFromSet(Set<Racun> racuni) {
		List<Racun> ra = new ArrayList<Racun>(racuni);
		for(Racun obj: ra) {
			
			this.racuni.add(new RacunDTO(obj));	
		}
			
	}
    
    public void setAktivniRacuniListFromSet(Set<Racun> racuni) {
		List<Racun> ra = new ArrayList<Racun>(racuni);
		for(Racun obj: ra) {
			if(!obj.isIzbrisan() && obj.isOdobren()) {
				this.racuni.add(new RacunDTO(obj));	
			}	
		}
			
	}
}