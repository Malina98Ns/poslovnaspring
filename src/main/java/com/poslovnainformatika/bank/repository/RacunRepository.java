package com.poslovnainformatika.bank.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovnainformatika.bank.model.Racun;

public interface RacunRepository  extends JpaRepository<Racun, Long>{

	List<Racun> getByBankaIdAndOdobrenAndIzbrisan(long id, boolean odobren, boolean izbrisan);
	
	List<Racun> getByKlijentIdAndOdobrenAndIzbrisan(long id, boolean odobren, boolean izbrisan);
	
	List<Racun> getByBankaIdAndKlijentId(long bankaId, long kid);
	
	Racun getByBrojRacuna(String brojRacuna);
	
	List<Racun> getByBankaIdAndKlijentIdAndOdobrenAndIzbrisan(long bid, long kid, boolean odobren, boolean izbrisan);
	
	
}
