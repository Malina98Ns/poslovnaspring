package com.poslovnainformatika.bank.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import com.poslovnainformatika.bank.model.enums.UlogaKorisnika;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Korisnik {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = false)
    private String korisnickoIme;

    @Column(unique = false, nullable = false)
    private String lozinka;

    @Column(unique = false, nullable = false)
    private UlogaKorisnika uloga;

    @OneToOne
    private Klijent klijent;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Banka banka;
}