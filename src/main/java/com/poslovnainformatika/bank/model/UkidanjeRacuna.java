package com.poslovnainformatika.bank.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UkidanjeRacuna {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = false)
    private String obrazlozenje;

    @Column(unique = false, nullable = false)
    private Date datumKreiranja;

    @Column(unique = false, nullable = false)
    private boolean zavrseno;

    @OneToOne
    private Racun racunZaUkidanje;

    @OneToOne
    private Racun racunZaPrenosNovca;
}