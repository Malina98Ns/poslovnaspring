package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.model.Banka;

public interface BankaServiceInterface {
	
	List<Banka> findAll();
	
	Banka findOne(long id);
	
	Banka save(Banka banka);
	
	void remove(long id);

}
