package com.poslovnainformatika.bank.service;


import java.util.Date;
import java.util.List;

import com.poslovnainformatika.bank.dto.NalogDTO;
import com.poslovnainformatika.bank.model.Drzava;
import com.poslovnainformatika.bank.model.Nalog;
import com.poslovnainformatika.bank.model.Racun;
import com.poslovnainformatika.bank.model.Valuta;



public interface NalogServiceInterface {

	List<Nalog> findAll();
	
	Nalog findOne(long id);
	
	Nalog save(Nalog nalog);
	
	void remove(long id);

	List<Nalog> naloziDnevnogStanjaZaRacunPoDatumu(Date odDatum, Date doDatum, long rid);
	
	void saveTransakcija(NalogDTO dto,Racun racunDuznika,Racun racunPrimaoca, Drzava drzava, Valuta valuta, int param);
	
}
