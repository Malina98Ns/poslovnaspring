package com.poslovnainformatika.bank.model;

import com.poslovnainformatika.bank.model.enums.TipKlijenta;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Klijent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = false)
    private String ime;

    @Column(unique = false, nullable = false)
    private String prezime;

    @Column(unique = false, nullable = false)
    private String jmbg;

    @Column(unique = false, nullable = false)
    private String telefon;

    @Column(unique = false, nullable = false)
    private String adresa;

    @Column(unique = false, nullable = false)
    private TipKlijenta tipKlijenta;

    @Column(unique = false, nullable = false)
    private boolean odobren;

    @OneToOne
    private Korisnik korisnik;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Delatnost delatnost;

    @OneToMany(mappedBy = "klijent", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Racun> racuni = new HashSet<Racun>();
}