package com.poslovnainformatika.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IzvestajDnevnoStanjeRacunaDTO {

    private String brojIzvoda;
    private Date datumPrometa;
    private double novoStanje;
    private double prethodnoStanje;
    private double iznos;
    private String duznik;
    private String primaoc;
    private String svrhaPlacanja;
    private String vrstaPlacanja;
    private String valuta;
}