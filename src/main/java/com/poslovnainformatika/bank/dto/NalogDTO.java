package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.Nalog;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NalogDTO {

    private long id;
    private String primaoc;
    private String svrhaPlacanja;
    private String duznik;
    private Date datumPrijema;
    private Date datumValute;
    private String modelZaduzenja;
    private String pozivNaBrojZaduzenja;
    private String modelOdobrenja;
    private String pozivNaBrojOdobrenja;
    private boolean hitno;
    private double iznos;
    private boolean status;
    private String tipGreske;
    private RacunDTO racunDuznika;
    private RacunDTO racunPrimaoca;
    private String vrstaPlacanja;

    private DrzavaDTO drzava;
    private DnevnoStanjeDTO dnevnoStanje;
    private ValutaDTO valuta;

    public NalogDTO(Nalog obj) {
        id = obj.getId();
        primaoc = obj.getPrimaoc();
        svrhaPlacanja = obj.getSvrhaPlacanja();
        duznik = obj.getDuznik();
        datumPrijema = obj.getDatumPrijema();
        datumValute = obj.getDatumValute();
        modelZaduzenja = obj.getModelZaduzenja();
        pozivNaBrojZaduzenja = obj.getPozivNaBrojZaduzenja();
        modelOdobrenja = obj.getModelOdobrenja();
        hitno = obj.isHitno();
        iznos = obj.getIznos();
        status = obj.isStatus();
        tipGreske = obj.getTipGreske();
        vrstaPlacanja = obj.getVrstaPlacanja();

        if (obj.getRacunDuznika() != null)
            racunDuznika = new RacunDTO(obj.getRacunDuznika());
        if (obj.getRacunPrimaoca() != null)
            racunPrimaoca = new RacunDTO(obj.getRacunPrimaoca());
        if (obj.getDrzava() != null)
            drzava = new DrzavaDTO(obj.getDrzava());
        if (obj.getDnevnoStanje() != null)
            dnevnoStanje = new DnevnoStanjeDTO(obj.getDnevnoStanje());
        if (obj.getValuta() != null)
            valuta = new ValutaDTO(obj.getValuta());
    }
}
