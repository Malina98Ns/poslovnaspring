package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.Delatnost;
import com.poslovnainformatika.bank.model.Klijent;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DelatnostDTO {

    private long id;
    private String sifra;
    private String naziv;
    private List<KlijentDTO> klijenti = new ArrayList<>();

    public DelatnostDTO(Delatnost obj) {
        id = obj.getId();
        sifra = obj.getSifra();
        naziv = obj.getNaziv();
    }

    public void setKlijentiListFromSet(Set<Klijent> klijenti) {
        List<Klijent> kl = new ArrayList<Klijent>(klijenti);
        for(Klijent obj : kl)
            this.klijenti.add(new KlijentDTO(obj));
    }
}