package com.poslovnainformatika.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovnainformatika.bank.model.Banka;

public interface BankaRepository extends JpaRepository<Banka, Long> {

}
