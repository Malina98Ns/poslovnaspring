package com.poslovnainformatika.bank.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Banka {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = false)
    private String sifra;

    @Column(unique = false, nullable = false)
    private String naziv;

    @Column(unique = false, nullable = true)
    private String adresa;

    @Column(unique = false, nullable = true)
    private String email;

    @Column(unique = false, nullable = true)
    private String web;

    @Column(unique = false, nullable = true)
    private String telefon;

    @Column(unique = false, nullable = true)
    private String fax;

    @Column(unique = false, nullable = true)
    private String swift;

    @Column(unique = false, nullable = true)
    private String obracunskiRacun;

    @OneToMany(mappedBy = "banka")
    private Set<Racun> racuni = new HashSet<Racun>();

    @OneToMany(mappedBy = "banka")
    private Set<KursnaLista> kursneListe = new HashSet<KursnaLista>();

    @OneToMany(mappedBy = "banka")
    private Set<Korisnik> izvrsioci = new HashSet<Korisnik>();

    @OneToMany(mappedBy = "bankaDuznika")
    private Set<MedjubankarskiPrenos> medjubankarskiPrenosDuznik = new HashSet<MedjubankarskiPrenos>();

    @OneToMany(mappedBy = "bankaPrimaoca")
    private Set<MedjubankarskiPrenos> medjubankarskiPrenosPrimaoc = new HashSet<MedjubankarskiPrenos>();
}