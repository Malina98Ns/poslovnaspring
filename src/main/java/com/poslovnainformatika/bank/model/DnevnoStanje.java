package com.poslovnainformatika.bank.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Getter
@Setter
public class DnevnoStanje {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = true)
    private String brojIzvoda;

    @Column(unique = false, nullable = true)
    private Date datumPrometa;

    @Column(unique = false, nullable = true)
    private double prethodnoStanje;

    @Column(unique = false, nullable = true)
    private double prometUKorist;

    @Column(unique = false, nullable = true)
    private double prometNaTeret;

    @Column(unique = false, nullable = true)
    private double novoStanje;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Racun racun;

    @OneToMany(mappedBy = "dnevnoStanje")
    private Set<Nalog> nalozi = new HashSet<Nalog>();
}