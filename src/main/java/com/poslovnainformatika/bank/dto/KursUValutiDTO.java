package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.KursUValuti;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KursUValutiDTO {

    private long id;
    private long redniBroj;
    private double kupovni;
    private double srednji;
    private double prodajni;
    private ValutaDTO osnovnaValuta;
    private ValutaDTO sporednaValuta;
    private KursnaListaDTO kursnaLista;

    public KursUValutiDTO(KursUValuti obj) {
        id = obj.getId();
        redniBroj = obj.getRedniBroj();
        kupovni = obj.getKupovni();
        srednji = obj.getSrednji();
        prodajni = obj.getProdajni();
        if(obj.getOsnovnaValuta() != null)
            osnovnaValuta = new ValutaDTO(obj.getOsnovnaValuta());
        if(obj.getSporednaValuta() != null)
            sporednaValuta = new ValutaDTO(obj.getSporednaValuta());
        if(obj.getKursnaLista() != null)
            kursnaLista = new KursnaListaDTO(obj.getKursnaLista());
    }
}
