package com.poslovnainformatika.bank.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Valuta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = false)
    private String sifra;

    @Column(unique = false, nullable = false)
    private String naziv;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Drzava drzava;

    @OneToMany(mappedBy = "valuta")
    private Set<Racun> racuni = new HashSet<Racun>();

    ///npr. ako je ova valuta RSD, lista kurs u valuti gde se gleda od RSD (osnovna)
    @OneToMany(mappedBy = "osnovnaValuta")
    private Set<KursUValuti> kursKaoOsnovnaValuta = new HashSet<KursUValuti>();

    //npr. ako je ova valuta RSD, lista kurs u valuti gde se gleda prema RSD (sporedna)
    @OneToMany(mappedBy = "sporednaValuta")
    private Set<KursUValuti> kursKaoSporednaValuta = new HashSet<KursUValuti>();

    @OneToMany(mappedBy = "valuta")
    private Set<Nalog> nalozi = new HashSet<Nalog>();
}