package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.KursUValuti;
import com.poslovnainformatika.bank.model.Nalog;
import com.poslovnainformatika.bank.model.Racun;
import com.poslovnainformatika.bank.model.Valuta;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ValutaDTO {

    private long id;
    private String sifra;
    private String naziv;
    private DrzavaDTO drzava;
    private List<RacunDTO> racuni = new ArrayList<>();
    private List<KursUValutiDTO> kursKaoOsnovnaValuta = new ArrayList<KursUValutiDTO>();
    private List<KursUValutiDTO> kursKaoSporednaValuta = new ArrayList<KursUValutiDTO>();
    private List<NalogDTO> nalozi = new ArrayList<NalogDTO>();

    public ValutaDTO(Valuta obj) {
        id = obj.getId();
        sifra = obj.getSifra();
        naziv = obj.getNaziv();

        if(obj.getDrzava() != null)
            drzava = new DrzavaDTO(obj.getDrzava());
    }
    
    public void setRacuniListFromSet(Set<Racun> racuni) {
		List<Racun> rl = new ArrayList<Racun>(racuni);
		for(Racun obj : rl)
			this.racuni.add(new RacunDTO(obj));
	}

	public void setKursKaoOsnovnaListFromSet(Set<KursUValuti> kurseviUValuti) {
		List<KursUValuti> kuvl = new ArrayList<KursUValuti>(kurseviUValuti);
		for(KursUValuti obj : kuvl)
			this.kursKaoOsnovnaValuta.add(new KursUValutiDTO(obj));
	}
	
	public void setKursKaoSporednaListFromSet(Set<KursUValuti> kurseviUValuti) {
		List<KursUValuti> kuvl = new ArrayList<KursUValuti>(kurseviUValuti);
		for(KursUValuti obj : kuvl)
			this.kursKaoSporednaValuta.add(new KursUValutiDTO(obj));
	}
	
	public void setNaloziListFromSet(Set<Nalog> nalozi) {
		List<Nalog> nl = new ArrayList<Nalog>(nalozi);
		for(Nalog obj : nl)
			this.nalozi.add(new NalogDTO(obj));
	}
}