package com.poslovnainformatika.bank.authmodel;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AuthenticationRequest {

    private String korisnickoIme;
    private String lozinka;
    private String uloga;
}
