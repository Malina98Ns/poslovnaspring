package com.poslovnainformatika.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovnainformatika.bank.model.Poruka;


public interface PorukaRepository extends JpaRepository<Poruka, Long> {

}
