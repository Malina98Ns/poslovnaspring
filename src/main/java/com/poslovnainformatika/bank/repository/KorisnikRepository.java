package com.poslovnainformatika.bank.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovnainformatika.bank.model.Korisnik;

public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {

	Korisnik findByKorisnickoIme(String korIme);
}
