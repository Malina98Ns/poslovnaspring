package com.poslovnainformatika.bank.model.enums;

public enum TipTransfera {

	RTGS,
	CLEARING
}