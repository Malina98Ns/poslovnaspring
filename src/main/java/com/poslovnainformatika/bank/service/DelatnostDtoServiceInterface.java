package com.poslovnainformatika.bank.service;

import com.poslovnainformatika.bank.dto.DelatnostDTO;
import com.poslovnainformatika.bank.model.Delatnost;

import java.util.List;

public interface DelatnostDtoServiceInterface {

    List<DelatnostDTO> getAllDTOs(List<Delatnost> delatnosti);

    DelatnostDTO getDelatnostDTO(Delatnost del);
}