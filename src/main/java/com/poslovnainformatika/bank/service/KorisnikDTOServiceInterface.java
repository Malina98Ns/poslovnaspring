package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.dto.KorisnikDTO;
import com.poslovnainformatika.bank.model.Korisnik;

public interface 	KorisnikDTOServiceInterface {

	List<KorisnikDTO> getAllDTOs(List<Korisnik> korisnici);
	
	KorisnikDTO getKorisnikDTO(Korisnik korisnik);
	
}
