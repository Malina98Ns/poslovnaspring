package com.poslovnainformatika.bank.controller;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;

import com.poslovnainformatika.bank.service.IzvestajIObradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value="api/izvestaj")
public class IzvestajiObradeController {

    @Autowired
    IzvestajIObradeService is;

    @RequestMapping(value="/download/{bankaId}/{klijentId}",method = RequestMethod.GET)
    public ResponseEntity<?> downloadIzvestajBanke(@PathVariable long bankaId,@PathVariable long klijentId) throws ParseException, IOException{

        File pdf = is.getPdfIzvestaj(bankaId,klijentId);
        if(pdf != null) {
            Path filePath = pdf.toPath();
            String mimeType = Files.probeContentType(filePath);
            URI uri = pdf.toURI();
            byte[] fileAsByteArray = Files.readAllBytes(Paths.get(uri));

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(mimeType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+pdf.getName()+"\"")
                    .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "*")
                    .body(new ByteArrayResource(fileAsByteArray));
        }else
            return new ResponseEntity<>("Greska prilikom preuzimanja fajla!",HttpStatus.NOT_FOUND);

    }

    @RequestMapping(value="/download/{from}/{to}/{racunId}/{klijentId}",method = RequestMethod.GET)
    public ResponseEntity<?> downloadIzvestajAndExportIzvestajToXml(@PathVariable long from,
                                                                    @PathVariable long to,
                                                                    @PathVariable long racunId,
                                                                    @PathVariable long klijentId) throws ParseException, IOException{

        Date odDatum = null;
        Date doDatum = null;
        odDatum = is.getDateFromMillis(from);
        doDatum = is.getDateFromMillis(to);

        File pdf = is.getPdfIzvestajRacuna(odDatum, doDatum, racunId,klijentId);
        if(pdf != null) {
            Path filePath = pdf.toPath();
            String mimeType = Files.probeContentType(filePath);
            URI uri = pdf.toURI();
            byte[] fileAsByteArray = Files.readAllBytes(Paths.get(uri));

            return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(mimeType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\""+pdf.getName()+"\"")
                    .header(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, "*")
                    .body(new ByteArrayResource(fileAsByteArray));
        }else
            return new ResponseEntity<>("Greska prilikom preuzimanja fajla!",HttpStatus.NOT_FOUND);

    }
}