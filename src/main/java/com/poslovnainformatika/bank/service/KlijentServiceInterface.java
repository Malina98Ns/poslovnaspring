package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.model.Klijent;

public interface KlijentServiceInterface {
	
	List<Klijent> findAll();
	
	Klijent findOne(long id);
	
	Klijent save(Klijent klijent);
	
	void remove(long id);
	
	Klijent getByKorisnikId(long id);
	
	List<Klijent> getAllWithAktivanRacunByBanka(long id);
}
