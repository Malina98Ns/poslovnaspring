package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.Drzava;
import com.poslovnainformatika.bank.model.Nalog;
import com.poslovnainformatika.bank.model.Valuta;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DrzavaDTO {

    private long id;
    private String sifra;
    private String naziv;
    private List<ValutaDTO> valute = new ArrayList<>();
    private List<NalogDTO> nalozi = new ArrayList<NalogDTO>();

    public DrzavaDTO(Drzava obj) {
        id = obj.getId();
        sifra = obj.getSifra();
        naziv = obj.getNaziv();
    }

    public void setValuteListFromSet(Set<Valuta> valute) {
		List<Valuta> vl = new ArrayList<Valuta>(valute);
		for(Valuta obj : vl)
			this.valute.add(new ValutaDTO(obj));
	}

	public void setNaloziListFromSet(Set<Nalog> nalozi) {
		List<Nalog> nl = new ArrayList<Nalog>(nalozi);
		for(Nalog obj : nl)
			this.nalozi.add(new NalogDTO(obj));
	}

}
