package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.Poruka;
import com.poslovnainformatika.bank.model.enums.TipPoruke;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PorukaDTO {

    private long id;
    private TipPoruke tipPoruke;
    private double iznos;
    private String swiftDuznikaBanke;
    private String swiftPrimaocaBanke;
    private String obracunskiRacunDuznikaBanke;
    private String obracunskiRacunPrimaocaBanke;
    private String duznik;
    private String svrhaPlacanja;
    private String primalac;
    private Date datumNaloga;
    private Date datumValute;
    private String racunDuznika;
    private String racunPrimaoca;
    private String modelOdobrenja;
    private String modelZaduzenja;
    private String pozivNaBrojOdobrenja;
    private String pozivNaBrojZaduzenja;
    private String sifraValute;
    private PorukaDTO poruka;

    public PorukaDTO (Poruka p) {
        this.id = p.getId();
        this.tipPoruke = p.getTipPoruke();
        this.iznos = p.getIznos();
        this.swiftDuznikaBanke = p.getSwiftDuznikaBanke();
        this.swiftPrimaocaBanke = p.getSwiftPrimaocaBanke();
        this.obracunskiRacunDuznikaBanke = p.getObracunskiRacunDuznikaBanke();
        this.obracunskiRacunPrimaocaBanke = p.getObracunskiRacunPrimaocaBanke();
        this.duznik = p.getDuznik();
        this.svrhaPlacanja = p.getSvrhaPlacanja();
        this.primalac = p.getPrimalac();
        this.datumNaloga = p.getDatumNaloga();
        this.datumValute = p.getDatumValute();
        this.racunDuznika = p.getRacunDuznika();
        this.racunPrimaoca = p.getRacunPrimaoca();
        this.modelOdobrenja = p.getModelOdobrenja();
        this.modelZaduzenja = p.getModelZaduzenja();
        this.pozivNaBrojOdobrenja = p.getPozivNaBrojOdobrenja();
        this.pozivNaBrojZaduzenja = p.getPozivNaBrojZaduzenja();
        this.sifraValute = p.getSifraValute();
        if(p.getPoruka2() != null) {
            this.poruka = new PorukaDTO(p);
        }

    }
}
