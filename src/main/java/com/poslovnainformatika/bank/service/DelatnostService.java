package com.poslovnainformatika.bank.service;

import com.poslovnainformatika.bank.dto.DelatnostDTO;
import com.poslovnainformatika.bank.model.Delatnost;
import com.poslovnainformatika.bank.repository.DelatnostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class DelatnostService implements DelatnostDtoServiceInterface , DelatnostServiceInterface{

    @Autowired
    DelatnostRepository dr;

    @Override
    public List<Delatnost> findAll() {
        return dr.findAll();
    }
    @Override
    public Delatnost findOne(long id) {
        return dr.findById(id).orElse(null);
    }
    @Override
    @Transactional(readOnly = false)
    public Delatnost save(Delatnost ban) {
        return dr.save(ban);
    }
    @Override
    @Transactional(readOnly = false)
    public void remove(long id) {
        dr.deleteById(id);
    }
    @Override
    public List<DelatnostDTO> getAllDTOs(List<Delatnost> delatnosti) {

        List<DelatnostDTO> dtos = new ArrayList<>();
        for (Delatnost d : delatnosti) {
            DelatnostDTO dto = new DelatnostDTO(d);
            dto.setKlijentiListFromSet(d.getKlijenti());
            dtos.add(dto);
        }
        return dtos;
    }
    @Override
    public DelatnostDTO getDelatnostDTO(Delatnost del) {
        DelatnostDTO dto = new DelatnostDTO(del);
        dto.setKlijentiListFromSet(del.getKlijenti());
        return dto;
    }
}