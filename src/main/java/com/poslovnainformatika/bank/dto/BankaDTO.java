package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.Banka;
import com.poslovnainformatika.bank.model.Korisnik;
import com.poslovnainformatika.bank.model.KursnaLista;
import com.poslovnainformatika.bank.model.Racun;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BankaDTO {

    private long id;
    private String sifra;
    private String naziv;
    private String adresa;
    private String email;
    private String web;
    private String telefon;
    private String fax;
    private String swift;
    private String obracunskiRacun;
    private List<RacunDTO> racuni = new ArrayList<>();
    private List<KursnaListaDTO> kursneListe = new ArrayList<KursnaListaDTO>();
    private List<KorisnikDTO> izvrsioci = new ArrayList<KorisnikDTO>();
    private List<MedjuBankarskiPrenosDTO> medjubankarskiPrenosDuznik = new ArrayList<MedjuBankarskiPrenosDTO>();
    private List<MedjuBankarskiPrenosDTO> medjubankarskiPrenosPrimaoc = new ArrayList<MedjuBankarskiPrenosDTO>();

    public BankaDTO(Banka obj) {
        id = obj.getId();
        sifra = obj.getSifra();
        naziv = obj.getNaziv();
        adresa = obj.getAdresa();
        email = obj.getEmail();
        web = obj.getWeb();
        telefon = obj.getTelefon();
        fax = obj.getFax();
        swift = obj.getSwift();
        obracunskiRacun = obj.getObracunskiRacun();
    }
    
    public void setRacuniListFromSet(Set<Racun> racuni) {
		List<Racun> rl = new ArrayList<Racun>(racuni);
		for(Racun obj : rl)
			this.racuni.add(new RacunDTO(obj));
	}

	public void setKursneListeListFromSet(Set<KursnaLista> kursneListe) {
		List<KursnaLista> kl = new ArrayList<KursnaLista>(kursneListe);
		for(KursnaLista obj : kl)
			this.kursneListe.add(new KursnaListaDTO(obj));
	}
	
	public void setIzvrsiociListFromSet(Set<Korisnik> izvrsioci) {
		List<Korisnik> il = new ArrayList<Korisnik>(izvrsioci);
		for(Korisnik obj : il)
			this.izvrsioci.add(new KorisnikDTO(obj));
	}
	
	

	public List<MedjuBankarskiPrenosDTO> getMedjubankarskiPrenosDuznik() {
		return medjubankarskiPrenosDuznik;
	}

	public List<MedjuBankarskiPrenosDTO> getMedjubankarskiPrenosPrimaoc() {
		return medjubankarskiPrenosPrimaoc;
	}
}