package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.dto.RacunDTO;
import com.poslovnainformatika.bank.model.Racun;

public interface RacunDTOServiceInterface {
	
	List<RacunDTO> getAllDTOs(List<Racun> racuni);
	
	List<RacunDTO> getAllAktivniDTOs(List<Racun> racuni);
	
	RacunDTO getRacunDTO(Racun r);
	
	
}
