package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.dto.ValutaDTO;
import com.poslovnainformatika.bank.model.Valuta;

public interface ValutaDTOServiceInterface {
	
	List<ValutaDTO> getAllDTOs(List<Valuta> valute);
	
	ValutaDTO getValutaDTO(Valuta valuta);
	
}
