package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.Korisnik;
import com.poslovnainformatika.bank.model.enums.UlogaKorisnika;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class KorisnikDTO {

    private long id;
    private String korisnickoIme;
    private String lozinka;
    private UlogaKorisnika uloga;
    private KlijentDTO klijent;
    private BankaDTO banka;

    public KorisnikDTO(Korisnik obj) {
        id = obj.getId();
        korisnickoIme = obj.getKorisnickoIme();
        lozinka = obj.getLozinka();
        uloga = obj.getUloga();
        if(obj.getBanka() != null)
            banka = new BankaDTO(obj.getBanka());
    }
}