package com.poslovnainformatika.bank.controller;

import java.util.List;


import com.poslovnainformatika.bank.dto.DelatnostDTO;
import com.poslovnainformatika.bank.model.Delatnost;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.poslovnainformatika.bank.dto.ValutaDTO;
import com.poslovnainformatika.bank.model.Drzava;
import com.poslovnainformatika.bank.model.Valuta;
import com.poslovnainformatika.bank.service.DrzavaService;
import com.poslovnainformatika.bank.service.ValutaService;

@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value="api/valuta")
public class ValutaController {
	
	@Autowired
	ValutaService vs;
	
	@Autowired
	DrzavaService ds;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<ValutaDTO>> getAll() {
		List<Valuta> valute = vs.findAll();	
		List<ValutaDTO> dtos = vs.getAllDTOs(valute);		
		
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<ValutaDTO> getById(@PathVariable long id){
		Valuta v = vs.findOne(id);
		
		if(v == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		
		ValutaDTO dto = vs.getValutaDTO(v);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<?> save(@RequestBody ValutaDTO dto){
		if(dto.getDrzava() == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		
		Valuta val = new Valuta();
		val.setNaziv(dto.getNaziv());
		val.setSifra(dto.getSifra());
		Drzava drzava = ds.findOne(dto.getDrzava().getId());	
		val.setDrzava(drzava);

		vs.save(val);
		return new ResponseEntity<>(new ValutaDTO(val), HttpStatus.OK);
	}

	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<?> update(@RequestBody ValutaDTO dto){
		Valuta valuta = vs.findOne(dto.getId());
		if(valuta == null)
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		valuta.setNaziv(dto.getNaziv());
		valuta.setSifra(dto.getSifra());
		Drzava drzava = ds.findOne(dto.getDrzava().getId());
		valuta.setDrzava(drzava);

		vs.save(valuta);
		return new ResponseEntity<>(new ValutaDTO(valuta), HttpStatus.OK);
	}


	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable long id){
		//admin brise
		Valuta valuta = vs.findOne(id);
		if (valuta != null){
			vs.remove(id);
			List<Valuta> valute = vs.findAll();
			List<ValutaDTO> dtos = vs.getAllDTOs(valute);
			
			return new ResponseEntity<>(dtos, HttpStatus.OK);
		} else		
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		
	}


}
