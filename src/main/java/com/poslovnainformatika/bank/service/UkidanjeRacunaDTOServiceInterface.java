package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.dto.UkidanjeRacunaDTO;
import com.poslovnainformatika.bank.model.UkidanjeRacuna;

public interface UkidanjeRacunaDTOServiceInterface {
	
	List<UkidanjeRacunaDTO> getAllDTOs(List<UkidanjeRacuna> ukidanjeRacuna);
	
	UkidanjeRacunaDTO getUkidanjeRacunaDTO(UkidanjeRacuna ur);

}
