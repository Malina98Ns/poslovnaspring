package com.poslovnainformatika.bank.service;

import java.util.Date;
import java.util.List;

import com.poslovnainformatika.bank.dto.IzvestajBankaRacuniDTO;
import com.poslovnainformatika.bank.dto.IzvestajDnevnoStanjeRacunaDTO;



public interface IzvestajIObradeServiceInterface {

	List<IzvestajDnevnoStanjeRacunaDTO> getAllIzvestajDnevnoStanjeRacunaDTOs(Date odDatum, Date doDatum, long rid);
	List<IzvestajBankaRacuniDTO> getAllIzvestajBankaRacuniDTOs(long bid, long kid);
}
