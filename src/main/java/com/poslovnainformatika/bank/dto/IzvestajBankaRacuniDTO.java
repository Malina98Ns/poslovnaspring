package com.poslovnainformatika.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class IzvestajBankaRacuniDTO {

    private String brojRacuna;
    private double stanje;
    private Date datumKreiranja;
}
