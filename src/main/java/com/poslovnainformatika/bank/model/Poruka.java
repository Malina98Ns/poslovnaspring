package com.poslovnainformatika.bank.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.poslovnainformatika.bank.model.enums.TipPoruke;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class Poruka {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = true)
    private TipPoruke tipPoruke;

    @Column(unique = false, nullable = true)
    private double iznos;

    @Column(unique = false, nullable = true)
    private String swiftDuznikaBanke;

    @Column(unique = false, nullable = true)
    private String swiftPrimaocaBanke;

    @Column(unique = false, nullable = true)
    private String obracunskiRacunDuznikaBanke;

    @Column(unique = false, nullable = true)
    private String obracunskiRacunPrimaocaBanke;

    @Column(unique = false, nullable = true)
    private String duznik;

    @Column(unique = false, nullable = true)
    private String svrhaPlacanja;

    @Column(unique = false, nullable = true)
    private String primalac;

    @Column(unique = false, nullable = true)
    private Date datumNaloga;

    @Column(unique = false, nullable = true)
    private Date datumValute;

    @Column(unique = false, nullable = true)
    private String racunDuznika;

    @Column(unique = false, nullable = true)
    private String racunPrimaoca;

    @Column(unique = false, nullable = true)
    private String modelOdobrenja;

    @Column(unique = false, nullable = true)
    private String modelZaduzenja;

    @Column(unique = false, nullable = true)
    private String pozivNaBrojOdobrenja;

    @Column(unique = false, nullable = true)
    private String pozivNaBrojZaduzenja;

    @Column(unique = false, nullable = true)
    private String sifraValute;

    @OneToOne
    private Poruka poruka2;
}