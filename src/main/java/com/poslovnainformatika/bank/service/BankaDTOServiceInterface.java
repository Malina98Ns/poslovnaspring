package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.dto.BankaDTO;
import com.poslovnainformatika.bank.model.Banka;

public interface BankaDTOServiceInterface {
	
	List<BankaDTO> getAllDTOs(List<Banka> banka);
	
	BankaDTO getBankaDTO(Banka banka);

}
