package com.poslovnainformatika.bank.model;

import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Entity
@NoArgsConstructor
@Getter
@Setter
public class Nalog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = true)
    private String primaoc;

    @Column(unique = false, nullable = true)
    private String svrhaPlacanja;

    @Column(unique = false, nullable = true)
    private String duznik;

    @Column(unique = false, nullable = true)
    private Date datumPrijema;

    @Column(unique = false, nullable = true)
    private Date datumValute;

    @Column(unique = false, nullable = true)
    private String modelZaduzenja;

    @Column(unique = false, nullable = true)
    private String pozivNaBrojZaduzenja;

    @Column(unique = false, nullable = true)
    private String modelOdobrenja;

    @Column(unique = false, nullable = true)
    private String pozivNaBrojOdobrenja;

    @Column(unique = false, nullable = true)
    private boolean hitno;

    @Column(unique = false, nullable = true)
    private double iznos;

    @Column(unique = false, nullable = true)
    private boolean status;

    @Column(unique = false, nullable = true)
    private String tipGreske;

    @OneToOne
    private Racun racunDuznika;

    @OneToOne
    private Racun racunPrimaoca;

    @Column(unique = false, nullable = true)
    private String vrstaPlacanja;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Drzava drzava;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private DnevnoStanje dnevnoStanje;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Valuta valuta;
}