package com.poslovnainformatika.bank.model.enums;

public enum TipKlijenta {

	FizickoLice,
	PravnoLice
}