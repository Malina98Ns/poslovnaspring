package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.DnevnoStanje;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DnevnoStanjeDTO {

    private long id;
    private String brojIzvoda;
    private Date datumPrometa;
    private double prethodnoStanje;
    private double prometUKorist;
    private double prometNaTeret;
    private double novoStanje;

    private RacunDTO racun;
    private List<NalogDTO> nalozi = new ArrayList<>();

    public DnevnoStanjeDTO(DnevnoStanje obj) {
        id = obj.getId();
        brojIzvoda = obj.getBrojIzvoda();
        datumPrometa = obj.getDatumPrometa();
        prethodnoStanje = obj.getPrethodnoStanje();
        prometUKorist = obj.getPrometUKorist();
        prometNaTeret = obj.getPrometNaTeret();
        novoStanje = obj.getNovoStanje();
        if(obj.getRacun() != null)
            racun = new RacunDTO(obj.getRacun());
    }
}
