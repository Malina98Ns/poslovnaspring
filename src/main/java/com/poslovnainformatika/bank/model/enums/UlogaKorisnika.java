package com.poslovnainformatika.bank.model.enums;

public enum UlogaKorisnika {

	ROLE_ADMIN,
	ROLE_KORISNIK,
	ROLE_IZVRSILAC
}