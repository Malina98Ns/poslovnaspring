package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.model.Korisnik;

public interface KorisnikServiceInterface {

	List<Korisnik> findAll();
	
	Korisnik findOne(long id);
	
	Korisnik save(Korisnik kor);
	
	void remove(long id);
	
	Korisnik findByKorisnickoIme(String korime);

	
}
