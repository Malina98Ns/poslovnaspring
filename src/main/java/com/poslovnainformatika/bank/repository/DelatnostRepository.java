package com.poslovnainformatika.bank.repository;

import com.poslovnainformatika.bank.model.Delatnost;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DelatnostRepository extends JpaRepository<Delatnost, Long>{

}