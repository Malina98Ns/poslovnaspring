package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.dto.KlijentDTO;
import com.poslovnainformatika.bank.model.Klijent;

public interface KlijentDTOServiceInterface {
	
	List<KlijentDTO> getAllDTOs(List<Klijent> klijenti);
	
	KlijentDTO getKlijentDTO (Klijent klijent);
	
	KlijentDTO getKlijentDTOWithActivniRacuni(Klijent klijent);

}
