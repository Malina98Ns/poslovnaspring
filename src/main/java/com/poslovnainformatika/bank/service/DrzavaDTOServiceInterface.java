package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.model.Drzava;

import com.poslovnainformatika.bank.dto.DrzavaDTO;


public interface DrzavaDTOServiceInterface {

	List<DrzavaDTO> getAllDTOs(List<Drzava> drzave);
	
	DrzavaDTO getDrzavaDTO(Drzava drzave);

}
