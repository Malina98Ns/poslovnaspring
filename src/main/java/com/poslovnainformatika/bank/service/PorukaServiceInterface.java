package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.model.Poruka;


public interface PorukaServiceInterface {

	List<Poruka> findAll();
	
	Poruka findOne(long id);
	
	Poruka save(Poruka poruka);
	
	void remove(long id);
	
	

	
}
