package com.poslovnainformatika.bank.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Drzava {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = false)
    private String sifra;

    @Column(unique = false, nullable = false)
    private String naziv;

    @OneToMany(mappedBy = "drzava")
    private Set<Valuta> valute = new HashSet<Valuta>();

    @OneToMany(mappedBy = "drzava")
    private Set<Nalog> nalozi = new HashSet<Nalog>();
}