package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.UkidanjeRacuna;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UkidanjeRacunaDTO {

    private long id;
    private String obrazlozenje;
    private Date datumKreiranja;
    private boolean zavrseno;

    private RacunDTO racunZaUkidanje;
    private RacunDTO racunZaPrenosNovca;

    public UkidanjeRacunaDTO(UkidanjeRacuna obj) {
        id = obj.getId();
        obrazlozenje = obj.getObrazlozenje();
        datumKreiranja = obj.getDatumKreiranja();
        zavrseno = obj.isZavrseno();

        if(obj.getRacunZaUkidanje() != null)
            racunZaUkidanje = new RacunDTO(obj.getRacunZaUkidanje());
        if(obj.getRacunZaPrenosNovca() != null)
            racunZaPrenosNovca = new RacunDTO(obj.getRacunZaPrenosNovca());
    }

}
