INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Privatan biznis', 'PB');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Fabrika', 'F');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Softver developer', 'SD');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Prehrambena Industrija', 'PI');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Apatinska Pivara', 'AP');
INSERT INTO `ebank`.`delatnost` (`naziv`, `sifra`) VALUES ('Porno Mafija', 'PM');

INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Srbija', 'SRB');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Hrvatska', 'HR');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Crna Gora', 'CG');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Slovenija', 'SLO');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Grcka', 'GR');
INSERT INTO `ebank`.`drzava` (`naziv`, `sifra`) VALUES ('Rumunija', 'RU');
