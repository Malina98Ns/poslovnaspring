package com.poslovnainformatika.bank.service;

import java.util.Date;
import java.util.List;

import com.poslovnainformatika.bank.model.DnevnoStanje;




public interface DnevnoStanjeServiceInterface {

	List<DnevnoStanje> findAll();
	
	DnevnoStanje findOne(long id);
	
	DnevnoStanje save(DnevnoStanje ds);
	
	void remove(long id);
	
	List<DnevnoStanje> dnevnoStanjeZaDatum(Date datum, long rid);

	int countDnevnoStanje();
	
}
