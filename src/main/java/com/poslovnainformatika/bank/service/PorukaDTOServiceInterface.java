package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.dto.PorukaDTO;
import com.poslovnainformatika.bank.model.Poruka;


public interface PorukaDTOServiceInterface {
	
	List<PorukaDTO> getAllDTOs(List<Poruka> poruke);
	
	PorukaDTO getPorukaDTO(Poruka poruka);
	
}
