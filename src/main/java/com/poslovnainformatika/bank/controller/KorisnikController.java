package com.poslovnainformatika.bank.controller;



import java.util.List;


import com.poslovnainformatika.bank.model.Banka;
import com.poslovnainformatika.bank.model.enums.UlogaKorisnika;
import com.poslovnainformatika.bank.util.PasswordBCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.poslovnainformatika.bank.dto.KorisnikDTO;
import com.poslovnainformatika.bank.model.Korisnik;
import com.poslovnainformatika.bank.service.BankaService;
import com.poslovnainformatika.bank.service.KorisnikService;

@RestController
@CrossOrigin(origins ="*",allowedHeaders = "*")
@RequestMapping(value="api/korisnik")
public class KorisnikController {

	@Autowired
	KorisnikService ks;

	@Autowired
	BankaService bs;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<KorisnikDTO>> getAll() {
		List<Korisnik> korisnici = ks.findAll();
		List<KorisnikDTO> dtos = ks.getAllDTOs(korisnici);
		return new ResponseEntity<>(dtos, HttpStatus.OK);
	}

	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<KorisnikDTO> getById(@PathVariable long id){
		Korisnik kor = ks.findOne(id);
		if(kor == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		KorisnikDTO dto = ks.getKorisnikDTO(kor);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@RequestMapping(value="/username/{username}", method=RequestMethod.GET)
	public ResponseEntity<KorisnikDTO> getByKorisnickoIme(@PathVariable String username){
		Korisnik kor = ks.findByKorisnickoIme(username);
		if(kor == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
		KorisnikDTO dto = ks.getKorisnikDTO(kor);
		return new ResponseEntity<>(dto, HttpStatus.OK);
	}

	@RequestMapping(method=RequestMethod.POST, consumes="application/json")
	public ResponseEntity<KorisnikDTO> save(@RequestBody KorisnikDTO korDTO){
		Korisnik kor = new Korisnik();
		kor.setKorisnickoIme(korDTO.getKorisnickoIme());
		kor.setLozinka(korDTO.getLozinka());
		kor.setUloga(korDTO.getUloga());
		if(korDTO.getUloga().equals(UlogaKorisnika.ROLE_IZVRSILAC)) {
			if(korDTO.getBanka() == null)
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			Banka banka = bs.findOne(korDTO.getBanka().getId());
			kor.setBanka(banka);
		}

		ks.save(kor);
		return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
	}

	@RequestMapping(method=RequestMethod.PUT, consumes="application/json")
	public ResponseEntity<KorisnikDTO> update(@RequestBody KorisnikDTO korDTO){
		Korisnik kor = ks.findOne(korDTO.getId());

		if(kor == null)
			return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);

		
		Banka b = bs.findOne(korDTO.getBanka().getId());
		kor.setLozinka(PasswordBCrypt.hashPassword(korDTO.getLozinka()));
		kor.setUloga(korDTO.getUloga());
		kor.setBanka(b);
		kor.setKorisnickoIme(korDTO.getKorisnickoIme());
		
		ks.save(kor);

		return new ResponseEntity<>(new KorisnikDTO(kor), HttpStatus.OK);
	}
	
}
