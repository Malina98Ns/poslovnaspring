package com.poslovnainformatika.bank.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.poslovnainformatika.bank.model.MedjubankarskiPrenos;


public interface MedjubankarskiPrenosRepository extends JpaRepository<MedjubankarskiPrenos, Long> {

}
