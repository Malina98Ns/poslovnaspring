package com.poslovnainformatika.bank.dto;

import com.poslovnainformatika.bank.model.DnevnoStanje;
import com.poslovnainformatika.bank.model.Racun;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RacunDTO {

    private long id;
    private String brojRacuna;
    private double stanje;
    private Date datumKreiranja;
    private boolean izbrisan;
    private boolean odobren;

    private UkidanjeRacunaDTO ukidanjeRacuna;

    private BankaDTO banka;
    private KlijentDTO klijent;
    private ValutaDTO valuta;

    private List<DnevnoStanjeDTO> dnevnoStanje = new ArrayList<>();
    private List<MedjuBankarskiPrenosDTO> medjubankarskiPrenosDuznik = new ArrayList<MedjuBankarskiPrenosDTO>();
    private List<MedjuBankarskiPrenosDTO> medjubankarskiPrenosPrimaoc = new ArrayList<MedjuBankarskiPrenosDTO>();

    private double rezervisanIznos;

    public RacunDTO(Racun obj) {
        id = obj.getId();
        brojRacuna = obj.getBrojRacuna();
        stanje = obj.getStanje();
        datumKreiranja = obj.getDatumKreiranja();
        izbrisan = obj.isIzbrisan();
        odobren = obj.isOdobren();
        rezervisanIznos = obj.getRezervisanIznos();

        if(obj.getBanka() != null)
            banka = new BankaDTO(obj.getBanka());
        if(obj.getKlijent() != null)
            klijent = new KlijentDTO(obj.getKlijent());
        if(obj.getValuta() != null)
            valuta = new ValutaDTO(obj.getValuta());
    }
    
    public void setDnevnoStanjeListFromSet(Set<DnevnoStanje> dnevnoStanje) {
		List<DnevnoStanje> dsl = new ArrayList<DnevnoStanje>(dnevnoStanje);
		for(DnevnoStanje obj : dsl)
			this.dnevnoStanje.add(new DnevnoStanjeDTO(obj));
	}
    
}