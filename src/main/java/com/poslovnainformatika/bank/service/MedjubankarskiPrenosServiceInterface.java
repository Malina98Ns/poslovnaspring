package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.dto.NalogDTO;
import com.poslovnainformatika.bank.model.Drzava;
import com.poslovnainformatika.bank.model.MedjubankarskiPrenos;
import com.poslovnainformatika.bank.model.Racun;
import com.poslovnainformatika.bank.model.Valuta;


public interface MedjubankarskiPrenosServiceInterface {

	List<MedjubankarskiPrenos> findAll();
	
	MedjubankarskiPrenos findOne(long id);
	
	MedjubankarskiPrenos save(MedjubankarskiPrenos medj);
	
	void remove(long id);
	
	void saveMedjubankarskiPrenos(NalogDTO dto,Racun racunDuznika,Racun racunPrimaoca,Drzava drzava,Valuta valuta);

	
}
