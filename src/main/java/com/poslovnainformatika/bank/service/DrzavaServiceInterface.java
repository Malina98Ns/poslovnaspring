package com.poslovnainformatika.bank.service;

import java.util.List;

import com.poslovnainformatika.bank.model.Drzava;



public interface DrzavaServiceInterface {

	List<Drzava> findAll();
	
	Drzava findOne(long id);
	
	Drzava save(Drzava drz);
	
	void remove(long id);

}
