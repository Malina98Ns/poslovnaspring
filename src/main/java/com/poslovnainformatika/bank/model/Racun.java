package com.poslovnainformatika.bank.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Racun {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = false, nullable = false)
    private String brojRacuna;

    @Column(unique = false, nullable = false)
    private double stanje;

    @Column(unique = false, nullable = false)
    private Date datumKreiranja;

    @Column(unique = false, nullable = false)
    private boolean izbrisan;

    @Column(unique = false, nullable = false)
    private boolean odobren;

    @Column(unique = false, nullable = false)
    private double rezervisanIznos;

    @OneToOne(mappedBy = "racunZaUkidanje")
    private UkidanjeRacuna ukidanjeRacuna;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Banka banka;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Klijent klijent;

    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
    private Valuta valuta;

    @OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "racun")
    private Set<DnevnoStanje> dnevnoStanje = new HashSet<DnevnoStanje>();

    @OneToMany(mappedBy = "racunDuznika")
    private Set<MedjubankarskiPrenos> medjubankarskiPrenosDuznik = new HashSet<MedjubankarskiPrenos>();

    @OneToMany(mappedBy = "racunPrimaoca")
    private Set<MedjubankarskiPrenos> medjubankarskiPrenosPrimaoc = new HashSet<MedjubankarskiPrenos>();
}